// DlgParam.cpp : 实现文件
//

#include "stdafx.h"
#include "SerailDialog.h"
#include "DlgParam.h"
#include "afxdialogex.h"


// CDlgParam 对话框

IMPLEMENT_DYNAMIC(CDlgParam, CDialogEx)

CDlgParam::CDlgParam(CWnd* pParent /*=NULL*/)
	: CDialogEx(CDlgParam::IDD, pParent)
	, m_nIpAdress(0)
	, m_strParam1(_T(""))
	, m_strParam2(_T(""))
	, m_strParam3(_T(""))
	, m_nNetMask(0)
	, m_nGateway(0)
{

	m_strMac = _T("");
}

CDlgParam::~CDlgParam()
{
}

void CDlgParam::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_IPAddress(pDX, IDC_IPADDRESS1, m_nIpAdress);
	DDX_Text(pDX, IDC_EDIT1, m_strParam1);
	DDX_Text(pDX, IDC_EDIT2, m_strParam2);
	DDX_Text(pDX, IDC_EDIT3, m_strParam3);
	DDX_IPAddress(pDX, IDC_IPADDRESS2, m_nNetMask);
	DDX_IPAddress(pDX, IDC_IPADDRESS3, m_nGateway);
	DDX_Text(pDX, IDC_EDIT4, m_strMac);
	DDV_MaxChars(pDX, m_strMac, 6);
}


BEGIN_MESSAGE_MAP(CDlgParam, CDialogEx)
	ON_BN_CLICKED(IDOK, &CDlgParam::OnBnClickedOk)
	ON_BN_CLICKED(IDC_BUT_SET, &CDlgParam::OnBnClickedButSet)
	ON_BN_CLICKED(IDC_BUT_GET, &CDlgParam::OnBnClickedButGet)
END_MESSAGE_MAP()


// CDlgParam 消息处理程序


void CDlgParam::OnBnClickedOk()
{
	UpdateData();
	TRACE("param1, param2, param3: %s,%s,%s",m_strParam1,m_strParam2,m_strParam3);
	TRACE("IP:%x",m_nIpAdress);


	CDialogEx::OnOK();
}

static BYTE buffer[1024];
void CDlgParam::OnBnClickedButSet()
{
	UpdateData();
	//SendMessage(hWnd, WM_USER_MY_OWN_MESSAGE);
	*(DWORD*)buffer = m_nIpAdress;
	*(DWORD*)(buffer+4) = m_nNetMask;
	*(DWORD*)(buffer+8) = m_nGateway;


	GetParent()->SendMessage(WM_ParaToMainDlg, (WPARAM)buffer, 0);
}


void CDlgParam::OnBnClickedButGet()
{
	// TODO: 在此添加控件通知处理程序代码
}
