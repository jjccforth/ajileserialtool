#pragma once


// CDlgParam 对话框

class CDlgParam : public CDialogEx
{
	DECLARE_DYNAMIC(CDlgParam)

public:
	CDlgParam(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CDlgParam();

// 对话框数据
	enum { IDD = IDD_PARAMETERS };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	DWORD m_nIpAdress;
	CString m_strParam1;
	CString m_strParam2;
	CString m_strParam3;
	afx_msg void OnBnClickedOk();
	DWORD m_nNetMask;
	DWORD m_nGateway;
	afx_msg void OnBnClickedButSet();
	afx_msg void OnBnClickedButGet();
	CString m_strMac;
};
