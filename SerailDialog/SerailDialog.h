
// SerailDialog.h : main header file for the PROJECT_NAME application
//

#pragma once

#ifndef __AFXWIN_H__
	#error "include 'stdafx.h' before including this file for PCH"
#endif

#include "resource.h"		// main symbols

#define WM_ParaToMainDlg	WM_APP+31

// CSerailDialogApp:
// See SerailDialog.cpp for the implementation of this class
//

class CSerailDialogApp : public CWinApp
{
public:
	CSerailDialogApp();

// Overrides
public:
	virtual BOOL InitInstance();

// Implementation

	DECLARE_MESSAGE_MAP()
};

extern CSerailDialogApp theApp;