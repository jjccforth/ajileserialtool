
// SerailDialogDlg.cpp : implementation file
//

#include "stdafx.h"
#include "SerailDialog.h"
#include "DlgParam.h"

#include "SerialDialogDlg.h"
#include "afxdialogex.h"
#include "crc.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#endif



// CAboutDlg dialog used for App About

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// Dialog Data
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CSerialDialogDlg dialog

#define PAGE_SIZE	256
#define SECTOR_SIZE 256*PAGE_SIZE
//8 Megabyte
#define LARGE_SIZE	0x800000
#if 0
static BYTE gBuffer[SECTOR_SIZE];
#else
static BYTE gBuffer[LARGE_SIZE];
#endif

CSerialDialogDlg::CSerialDialogDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CSerialDialogDlg::IDD, pParent)
	, m_nBaudIndex(_T(""))
	, m_strSendMessage(_T("HELLO Ajile"))
	, m_strCom(_T(""))
	, m_strParameter(_T(""))
	, m_strFilename(_T(""))
	, m_strPacketsSent(_T(""))
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	m_strCommand = _T("");
	m_nParam2 = 0;
	m_nCurrentReadSize = 0;
}

void CSerialDialogDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_RICHEDIT_RECV, m_ctlRecieved);
	DDX_LBString(pDX, IDC_LIST_BAUD, m_nBaudIndex);
	DDX_Text(pDX, IDC_EDIT_SEND, m_strSendMessage);
	DDX_CBString(pDX, IDC_CMB_COM, m_strCom);
	DDX_CBString(pDX, IDC_CMB_CMD, m_strCommand);
	DDV_MaxChars(pDX, m_strCommand, 10);
	DDX_CBString(pDX, IDC_EDIT_PARAMETER, m_strParameter);
	DDX_Text(pDX, IDC_STATIC_FILE, m_strFilename);
	DDX_Text(pDX, IDC_EDIT_PARAMETER2, m_nParam2);
	DDV_MinMaxInt(pDX, m_nParam2, 0, 65536);
	DDX_Text(pDX, IDC_STATIC_PACKSENT, m_strPacketsSent);
}

BEGIN_MESSAGE_MAP(CSerialDialogDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()

	ON_WM_SERIAL(OnSerialMsg)
	ON_MESSAGE(WM_ParaToMainDlg, OnParaToMainDlg)
	ON_BN_CLICKED(IDC_BUT_CONNECT, &CSerialDialogDlg::OnBnClickedButConnect)
	ON_BN_CLICKED(IDC_BUT_DISCONNECT, &CSerialDialogDlg::OnBnClickedButDisconnect)
	ON_BN_CLICKED(IDC_BUT_SEND, &CSerialDialogDlg::OnBnClickedButSend)
	ON_BN_CLICKED(ID_BUT_CLEAR, &CSerialDialogDlg::OnBnClickedButClear)
	ON_BN_CLICKED(IDC_BUT_AUTO, &CSerialDialogDlg::OnBnClickedButAuto)
	ON_BN_CLICKED(IDC_BUT_SENDCMD, &CSerialDialogDlg::OnBnClickedButSendcmd)
	ON_BN_CLICKED(IDC_BUT_SENDT, &CSerialDialogDlg::OnBnClickedButSendt)
	ON_BN_CLICKED(IDC_BUT_FILE, &CSerialDialogDlg::OnBnClickedButFile)
	ON_BN_CLICKED(IDC_BUT_HELP, &CSerialDialogDlg::OnBnClickedButHelp)
//	ON_STN_CLICKED(IDC_STATIC_FILE, &CSerialDialogDlg::OnStnClickedStaticFile)
	ON_BN_CLICKED(IDC_BUT_SENDT2, &CSerialDialogDlg::OnBnClickedButSendt2)
	ON_BN_CLICKED(IDC_BUT_PARAM, &CSerialDialogDlg::OnBnClickedButParam)
END_MESSAGE_MAP()

CString strCommands[] = {TEXT("HELLO"),TEXT("DF"),TEXT("WF"),TEXT("BP"),TEXT("BL"),TEXT("FRID"),
	TEXT("UF"),TEXT("EF"),TEXT("LP"),TEXT("BLMORE")};

#define NUM_OF_COMMANDS 10
// CSerialDialogDlg message handlers

BOOL CSerialDialogDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	m_bConnected = FALSE;
	EnDisableButton(m_bConnected);

	CListBox * ctrlBaurdIndex = (CListBox *) GetDlgItem(IDC_LIST_BAUD);
	ctrlBaurdIndex->ResetContent();
	ctrlBaurdIndex->AddString(TEXT("1200"));
	ctrlBaurdIndex->AddString(TEXT("2400"));
	ctrlBaurdIndex->AddString(TEXT("9600"));
	ctrlBaurdIndex->AddString(TEXT("14400"));
	ctrlBaurdIndex->AddString(TEXT("19200"));
	ctrlBaurdIndex->AddString(TEXT("38400"));
	ctrlBaurdIndex->AddString(TEXT("56000"));
	ctrlBaurdIndex->AddString(TEXT("57600"));
	ctrlBaurdIndex->AddString(TEXT("115200"));
	//ctrlBaurdIndex->SetSel(7,TRUE);
	ctrlBaurdIndex->SetCurSel(8);

	CComboBox * ctrlCom = (CComboBox*) GetDlgItem(IDC_CMB_COM);
	CString strCom;
	for(int i = 0; i<40;i++){
		//strCom.Format(_T("COM%d\n"),i);

      if (i < 10)
          strCom.Format(_T("COM%d"), i);
        else
          strCom.Format(_T("\\\\.\\COM%d"), i); 

		TRACE(strCom);
		if( CSerial::CheckPort(strCom) == CSerial::EPortAvailable){

			TRACE(_T("Good for com:") + strCom + _T(",\n"));
			ctrlCom->AddString(strCom);		
		
		}
	}


	CComboBox * ctrlCommand = (CComboBox*) GetDlgItem(IDC_CMB_CMD);
	CString strCommand;
	for(int i = 0; i<NUM_OF_COMMANDS;i++){
		TRACE("size of array: %d", sizeof(strCommands));
		CString strTemp = strCommands[i];
		ctrlCommand->AddString(strCommands[i]);
	}

	m_bAuto = FALSE;
	m_pCurrent = m_szSerialBuffer;
	m_bExptResp = FALSE;

	CFont myFont;
	myFont.CreatePointFont(40, _T("Courier"));

	GetDlgItem(IDC_RICHEDIT_RECV)->SetFont(&myFont);
	//Init CRC table
	init_crc16_tab();

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CSerialDialogDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		//CAboutDlg dlgAbout;
		//dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CSerialDialogDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CSerialDialogDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

BOOL bWrite = FALSE;
#define EXPECTED_AMOUNT 3
LRESULT CSerialDialogDlg::OnSerialMsg (WPARAM wParam, LPARAM /*lParam*/)
{
	CSerial::EEvent eEvent = CSerial::EEvent(LOWORD(wParam));
	CSerial::EError eError = CSerial::EError(HIWORD(wParam));

	if (eError)
		TRACE(_T("An internal error occurred."));

	if (eEvent & CSerial::EEventBreak)
		TRACE(_T("Break detected on input."));

	if (eEvent & CSerial::EEventError)
		TRACE(_T("A line-status error occurred."));
	
	if (eEvent & CSerial::EEventRcvEv)
		TRACE(_T("Event character has been received."));

	if (eEvent & CSerial::EEventRing)
		TRACE(_T("Ring detected"));
	
	if (eEvent & CSerial::EEventSend)
		TRACE(_T("All data is send"));
	
	if (eEvent & CSerial::EEventCTS)
		TRACE(_T("CTS signal change"));
		//TRACE(_T("CTS signal change"), _T("CTS"), m_serial.GetCTS());
	
	if (eEvent & CSerial::EEventDSR)
		TRACE(_T("DSR signal change"));//, _T("DSR"), m_serial.GetDSR());
	
	if (eEvent & CSerial::EEventRLSD)
		TRACE(_T("RLSD"));
		//DisplayEventSetting(_T("RLSD signal change"), _T("RLSD"), m_serial.GetRLSD());
	if (eEvent & CSerial::EEventRecv)
	{
		// Create a clean buffer
		DWORD dwRead,dwTotal;
		const int nBuflen = sizeof(m_szSerialBuffer)-1 - (m_pCurrent -m_szSerialBuffer) ; //maximum size - used space

		// Obtain the data from the serial port
		do //Could read many time to exaust the incoming data
		{
			m_serial.Read(m_pCurrent,nBuflen,&dwRead);
			TRACE("Got data, with %d bytes\n",dwRead); 
			m_pCurrent += dwRead;
			dwTotal = m_pCurrent - m_szSerialBuffer;
			//Check if if 
			if(m_bExptResp == TRUE){
				//Get the response, process the response
				if(dwTotal< EXPECTED_AMOUNT)
					continue;
				//Send next packet
				TRACE("get %d bytes.1st 2nd and 3rd byte: 0x%02x 0x%02x 0x%02x\n",dwTotal, *m_szSerialBuffer, (BYTE) m_szSerialBuffer[1],(BYTE)m_szSerialBuffer[2] );
				m_bExptResp = FALSE;
				if(*m_szSerialBuffer != 0x54) //If not last packet
					SendNextPacket();
				else
					TRACE("Finished packets communication cycle\n");

				//continue;
			}

			*m_pCurrent = '\0';
		
			//Now there's a return
			m_pCurrent = m_szSerialBuffer; //reset pointer for next command

#ifdef _UNICODE
			// Convert the ANSI data to Unicode
			LPTSTR lpszData = LPTSTR(_alloca((dwTotal+1)*sizeof(TCHAR)));
			if (!::MultiByteToWideChar(CP_ACP, 0, m_szSerialBuffer, -1, lpszData, dwTotal+1))
				return 0;

			// Display the fetched string
			DisplayData(lpszData);
#else
			// Display the fetched string
			DisplayData(m_szSerialBuffer); //TRACE is broken when there's a %
#endif
		} while (dwRead == nBuflen);



	}

	return 0;
}


void CSerialDialogDlg::OnBnClickedButConnect()
{
	UpdateData();
	//int baud = m_nBaudIndex;
	CListBox * ctrlBaurdIndex = (CListBox *) GetDlgItem(IDC_LIST_BAUD);
	int nSel = 8;
	for (int i = 0; i<9;i++){
		if (TRUE == ctrlBaurdIndex->GetSel(i)){
			nSel = i;
			break;
		}

	}
	TRACE("selected: %d",nSel);

	if (m_serial.Open(m_strCom,this) != ERROR_SUCCESS)
	{
		AfxMessageBox(_T("Unable to open COM-port"),MB_ICONSTOP|MB_OK);
		//GetParent()->PostMessage(WM_CLOSE);
		return ;
	}
	m_bConnected = true;
	GetDlgItem(IDC_BUT_CONNECT)->EnableWindow(!m_bConnected);
	GetDlgItem(IDC_BUT_CONNECT)->EnableWindow(m_bConnected);
	SetupBaudRate(nSel);

	EnDisableButton(m_bConnected);
}


void CSerialDialogDlg::OnBnClickedButDisconnect()
{
	m_serial.Close();
	m_bConnected = false;

	EnDisableButton(m_bConnected);

}


void CSerialDialogDlg::OnBnClickedButSend()
{
	UpdateData();
	CString strAddedNewLine;
	strAddedNewLine.Format(TEXT("%s\n"),m_strSendMessage);
	if(m_strSendMessage.Find(_T("ECHO")) == -1)
		m_bExptResp = FALSE; //stop any continuous sending packet left by ECHO	
	
	CW2A pszA( strAddedNewLine );
	LPCSTR lpDataSend = pszA;

	TRACE("Got: %s",lpDataSend);


	DWORD dwDataLen = strlen(lpDataSend);

	if(m_bConnected){
		DWORD dwWritten = 0;
		m_serial.Write(lpDataSend,dwDataLen,&dwWritten);
	}


}



void CSerialDialogDlg::EnDisableButton(BOOL bConnected)
{
	GetDlgItem(IDC_BUT_CONNECT)->EnableWindow(!bConnected);
	GetDlgItem(IDC_BUT_DISCONNECT)->EnableWindow(bConnected);
	GetDlgItem(IDC_BUT_SEND)->EnableWindow(bConnected);
	GetDlgItem(IDC_BUT_SENDCMD)->EnableWindow(bConnected);
	GetDlgItem(IDC_BUT_SENDT2)->EnableWindow(bConnected);
	GetDlgItem(IDC_BUT_SENDT)->EnableWindow(bConnected);
	GetDlgItem(IDC_BUT_PARAM)->EnableWindow(bConnected);


}


void CSerialDialogDlg::SetupBaudRate(int nBaudrateIndex,int nDataIndex,int nParityIndex,int nStopIndex,int nHandshakingIndex){


	// Retrieve the settings from the page
	//UpdateData(true);

	// Determine baudrate
	CSerial::EBaudrate eBaudrate = CSerial::EBaudUnknown;
	switch (nBaudrateIndex)
	{
	case 0:  eBaudrate = CSerial::EBaud1200;   break;
	case 1:  eBaudrate = CSerial::EBaud2400;   break;
	case 2:  eBaudrate = CSerial::EBaud9600;   break;
	case 3:  eBaudrate = CSerial::EBaud14400;  break;
	case 4:  eBaudrate = CSerial::EBaud19200;  break;
	case 5:  eBaudrate = CSerial::EBaud38400;  break;
	case 6:  eBaudrate = CSerial::EBaud56000;  break;
	case 7:  eBaudrate = CSerial::EBaud57600;  break;
	case 8:  eBaudrate = CSerial::EBaud115200; break;
	default: ASSERT(false); break;
	}

	CSerial::EDataBits eDataBits = CSerial::EDataUnknown;
	switch (nDataIndex)
	{
	case 0:  eDataBits = CSerial::EData5; break;
	case 1:  eDataBits = CSerial::EData6; break;
	case 2:  eDataBits = CSerial::EData7; break;
	case 3:  eDataBits = CSerial::EData8; break;
	}

	CSerial::EParity eParity = CSerial::EParUnknown;
	switch (nParityIndex)
	{
	case 0: eParity = CSerial::EParNone;  break;
	case 1: eParity = CSerial::EParOdd;   break;
	case 2: eParity = CSerial::EParEven;  break;
	case 3: eParity = CSerial::EParMark;  break;
	case 4: eParity = CSerial::EParSpace; break;
	default: ASSERT(false); break;
	}

	CSerial::EStopBits eStopBits = CSerial::EStopUnknown;
	switch (nStopIndex)
	{
	case 0: eStopBits = CSerial::EStop1;   break;
	case 1: eStopBits = CSerial::EStop1_5; break;
	case 2: eStopBits = CSerial::EStop2;   break;
	default: ASSERT(false); break;
	}

	CSerial::EHandshake eHandshake = CSerial::EHandshakeUnknown;
	switch (nHandshakingIndex)
	{
	case 0: eHandshake = CSerial::EHandshakeOff;      break;
	case 1: eHandshake = CSerial::EHandshakeSoftware; break;
	case 2: eHandshake = CSerial::EHandshakeHardware; break;
	default: ASSERT(false); break;
	}

	// Setup the COM port
	if (m_serial.Setup(eBaudrate,eDataBits,eParity,eStopBits)){
		TRACE("Not good for setup baudrate)");
		return ;
	}
	TRACE("OK for setup baudrate)");

}


void CSerialDialogDlg::DisplayData (LPCTSTR pszData)
{
	// Format the selection as default text
	CHARFORMAT cf;
	cf.cbSize = sizeof(cf);
	cf.dwMask = CFM_COLOR;
	m_ctlRecieved.GetSelectionCharFormat(cf);
	cf.dwEffects |= CFE_AUTOCOLOR;
	m_ctlRecieved.SetSelectionCharFormat(cf);

	m_ctlRecieved.SetSel(-1,-1);
	m_ctlRecieved.ReplaceSel(pszData);

}

void CSerialDialogDlg::OnBnClickedButClear()
{
	m_ctlRecieved.SetSel(0,-1);
	m_ctlRecieved.ReplaceSel(TEXT(""));
	m_ctlRecieved.Clear();
}






DWORD CSerialDialogDlg::SendData(CString strDataForSend)
{

	CString strAddedNewLing;
	strAddedNewLing.Format(TEXT("%s"),strDataForSend);
	CW2A pszA(strAddedNewLing);
	LPCSTR lpDataSend =  pszA;
	DWORD dwDataLen = strlen(lpDataSend);
	DWORD dwWritten = 0;
	if(m_bConnected){

		m_serial.Write(lpDataSend,dwDataLen,&dwWritten);
	}
	return dwWritten;
}





void CSerialDialogDlg::OnBnClickedButAuto()
{

	m_bAuto = !m_bAuto;

}












void CSerialDialogDlg::OnBnClickedButSendcmd()
{
	UpdateData();
	CString strCommand;

	if(m_strParameter.IsEmpty())
		strCommand.Format(TEXT("%s\n"),m_strCommand);
	else
		strCommand.Format(TEXT("%s %s\n"),m_strCommand,m_strParameter);
	if(strCommand.Find(_T("ECHO")) == -1)
		m_bExptResp = FALSE; //stop any continuous sending packet left by ECHO
	SendData(strCommand);

	TRACE("OnBnClickedButSendcmd\n");
}

static BYTE btData[1024];
int nStart = 5;
void CSerialDialogDlg::OnBnClickedButSendt()
{
	UpdateData();
	for(int i = 0;i<1024;i++){
		btData[i] = nStart+i;
	}
	//Prepare the data,then
	m_nPackSeq = 0;
	m_nSecSeq = 0;
	SendNextPacket();
}

//#define TOTAL_PACKS 3
#define DATA_LEN	256
#define ETX		0x53
#define STX		0x50
#define CRC		0x55

#define HEADSIZE	7 

#define SEQ_OFFSET 		1
#define LEN_OFFSET		3
#define OFFSET_OFFSET	5


struct Header
{
   BYTE  cmd;
   WORD  sequenceInPackets;
   WORD  lengthInPackets;
   WORD  offsetInSector;
} header;  

int CSerialDialogDlg::SendNextPacket(void)
{
	//Header * pHeader = (Header *) m_btPacketBuffer ;
	//Because of alignment problem, sturct is not used here.

	//Packet format:
	//|STX|SeqLo|SeqHi|LenLo|LenHi|OffsetLo|OffsetHi|Data....|
	if(m_nPackSeq == m_nLengthAsPage - 1) //The last one
		m_btPacketBuffer[0]=ETX;
	else
		m_btPacketBuffer[0]=STX;

	* ((WORD *)( m_btPacketBuffer+SEQ_OFFSET)) = (WORD) m_nPackSeq;
	* ((WORD *)( m_btPacketBuffer+LEN_OFFSET)) = m_nLengthAsPage;
	* ((WORD *)( m_btPacketBuffer+OFFSET_OFFSET)) = m_nParam2 + m_nSecSeq; //m_nParam2 is start sector


	//Data
	DWORD nPackSeqInSector = m_nPackSeq%PAGE_SIZE;
	void * ptr;
	if (m_nCurrentReadSize == 0)
		ptr = btData + nPackSeqInSector*DATA_LEN;
	else //There's a file read.
		ptr = gBuffer + nPackSeqInSector*DATA_LEN;

	memcpy(m_btPacketBuffer+HEADSIZE, ptr , DATA_LEN );


	DWORD dwWritten;
	if(m_bConnected){
		m_serial.Write(m_btPacketBuffer,DATA_LEN + HEADSIZE,&dwWritten); 
		m_bExptResp = TRUE;

		if(m_nPackSeq == m_nLengthAsPage - 1){ //The last packet, extra bytes of CRC needs to be sent
			m_btPacketBuffer[0]=CRC;
			* ((WORD *)( m_btCrcBuffer+SEQ_OFFSET)) = 0xFFFF;
			* ((WORD *)( m_btCrcBuffer+LEN_OFFSET)) = 0xFFFF;
			* ((WORD *)( m_btCrcBuffer+OFFSET_OFFSET)) = 0xFFFF;
			memcpy(m_btCrcBuffer+HEADSIZE, &m_nCrc , 2);
			m_serial.Write(m_btCrcBuffer, 2 + HEADSIZE,&dwWritten);//CRC is only 2 bytes
		}
	}

	TRACE("Next Packet sent\n");

	m_nPackSeq ++;
	//Update Pointers && refresh buffer
	GoNextCheck();
	m_strPacketsSent.Format(_T("%d"),m_nPackSeq);
	UpdateData(FALSE);
	return 0;
}


void CSerialDialogDlg::OnBnClickedButFile()
{
	LPCTSTR szFilters = _T("All Files (*.*)|*.*||");

	CFileDialog fileDlg(TRUE,_T("*"), _T("*.*"), OFN_FILEMUSTEXIST| OFN_HIDEREADONLY, szFilters, this);

	// Display the file dialog. When user clicks OK, fileDlg.DoModal()
	// returns IDOK.
	if( fileDlg.DoModal ()==IDOK )
	{

		CFileException ex;
		m_strFilename = fileDlg.GetPathName();
		UpdateData(FALSE);
		//sourceFile.is.Close(); //In case it was open

		if (!m_fSourceFile.Open(m_strFilename,  CFile::modeRead | CFile::typeBinary, &ex))
		{
			TCHAR szError[1024];
			ex.GetErrorMessage(szError, 1024);
			TRACE(szError);
		}else{
			 m_bFileOpen = TRUE;
			 m_nFileLength = m_fSourceFile.GetLength();
			 m_nLengthAsPage = m_nFileLength/PAGE_SIZE+1;
			 m_nLengthAsSector = m_nFileLength/SECTOR_SIZE +1;

			 TRACE("File size: %u\n", m_nFileLength);
#if 0 
			 //Read 1st sector
			 memset(gBuffer,0xff,SECTOR_SIZE);
			 m_nCurrentReadSize = m_fSourceFile.Read(gBuffer, SECTOR_SIZE);
#else
			 memset(gBuffer,0xff,LARGE_SIZE);
			 m_nCurrentReadSize = m_fSourceFile.Read(gBuffer, LARGE_SIZE);
			 TRACE("Reading: %u\n", m_nCurrentReadSize);
#endif
			 m_nCrc = 0l;

			for(int i = 0;i< m_nCurrentReadSize;i++){
				update_crc_16(&m_nCrc,*(gBuffer+i));
			}
			TRACE("crc is 0x%04x",m_nCrc);
		}
	}

}


// Move sector pointer, refresh data buffer from file in case
int CSerialDialogDlg::GoNextCheck(void)
{
	if(m_nPackSeq%PAGE_SIZE == 0 ) //A new sector
	{
		memset(gBuffer,0xff,SECTOR_SIZE);
		m_nCurrentReadSize = m_fSourceFile.Read(gBuffer, SECTOR_SIZE);

		//for(int i = 0;i< m_nCurrentReadSize;i++){
		//	update_crc_16(&m_btCrc,*(gBuffer+i));
		//}
		m_nSecSeq++;
	}
	if( m_nPackSeq == m_nLengthAsPage) //
	{

		m_fSourceFile.Close();
		m_strFilename = "Done. Choose new file";
	}

	return 0;
}


void CSerialDialogDlg::OnBnClickedButHelp()
{
	CAboutDlg dlgAbout;
	dlgAbout.DoModal();
}

#define REDUNDUMCY 2
int CSerialDialogDlg::SendWhole(WORD nStartSector)
{
	//Header * pHeader = (Header *) m_btPacketBuffer ;
	//Because of alignment problem, sturct is not used here.

	//Packet format:
	//|STX|SeqLo|SeqHi|LenLo|LenHi|OffsetLo|OffsetHi|Data....|

	m_btPacketBuffer[0]=STX;

	* ((WORD *)( m_btPacketBuffer+SEQ_OFFSET)) = (WORD) nStartSector;
	* ((WORD *)( m_btPacketBuffer+LEN_OFFSET)) = m_nLengthAsPage;
	* ((WORD *)( m_btPacketBuffer+OFFSET_OFFSET)) = 0; //m_nParam2 is start sector
	
	DWORD timeout = 200;

	//Prepare command
	CString strCommand;
	//strCommand.Format(TEXT("UFX %ud\n",timeout));
	strCommand.Format(TEXT("UFX 20000\n"));

	CW2A pszA(strCommand);
	LPCSTR lpDataSend =  pszA;
	DWORD dwDataLen = strlen(lpDataSend);

	DWORD dwWritten;
	m_nPackSeq = 0;
	if(m_bConnected){
		//SendData(strCommand);
		m_serial.Write(lpDataSend,dwDataLen,&dwWritten);
		m_serial.Write(m_btPacketBuffer ,7, &dwWritten);  //Write a header
		//m_serial.Write(gBuffer ,m_nLengthAsPage*256,&dwWritten);  // then all the data
		///*
			for(int i = 0;i<m_nLengthAsPage;i++){
				m_serial.Write(gBuffer+i*256 ,256,&dwWritten);
				m_nPackSeq ++;
				TRACE("pk:%d/%d\n",i,m_nLengthAsPage);
				UpdateData(FALSE);
			}
		//*/
		//m_bExptResp = TRUE;
		TRACE("Data sent: %d\n",dwWritten);
	}
	UpdateData(FALSE);
	return 0;
}





void CSerialDialogDlg::OnBnClickedButSendt2()
{
	SendWhole(0);
}


void CSerialDialogDlg::OnBnClickedButParam()
{
	CDlgParam paramDlg;

	// Display the file dialog. When user clicks OK, fileDlg.DoModal()
	// returns IDOK.
	paramDlg.m_nIpAdress = m_nIp;
	paramDlg.m_strParam1 = m_strP1;
	paramDlg.m_strParam2 = m_strP2;
	paramDlg.m_strParam3 = m_strP3;
	if( paramDlg.DoModal ()==IDOK )
	{
		//Write block to 
		m_nIp = paramDlg.m_nIpAdress;
		m_strP1 = paramDlg.m_strParam1;
		m_strP2 = paramDlg.m_strParam2;
		m_strP3 = paramDlg.m_strParam3;
		m_nIp = paramDlg.m_nIpAdress;
		m_nNetMask = paramDlg.m_nNetMask;
		m_nGateway = paramDlg.m_nGateway;
		m_strMac = paramDlg.m_strMac;
	}
}

static BYTE buffer[1024];
LRESULT CSerialDialogDlg::OnParaToMainDlg(WPARAM wParam, LPARAM lParam)
{

	BYTE * pData = (BYTE *) wParam;

	memcpy(gBuffer, pData,1024);
	
	WORD nStartSector = 0xF8;//0xF80000/0x10000;
	m_nLengthAsPage = 1;
	SendWhole(nStartSector);
	TRACE(_T("ParaToMainDlg message received\n"));
	return 0;
}