
// SerailDialogDlg.h : header file
//
#include "stdafx.h"
#include "SerialMFC.h"

#pragma once

// CSerialDialogDlg dialog
class CSerialDialogDlg : public CDialogEx
{
// Construction
public:
	CSerialDialogDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	enum { IDD = IDD_SERAILDIALOG_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support

	CSerialMFC		m_serial;
	BOOL			m_bConnected;

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
	afx_msg LRESULT OnSerialMsg (WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnParaToMainDlg(WPARAM wParam, LPARAM lParam);
public:
	afx_msg void OnBnClickedButConnect();
	afx_msg void OnBnClickedButDisconnect();
	afx_msg void OnBnClickedButSend();

private:

void EnDisableButton(BOOL bConnected);
void SetupBaudRate(int nBaudrateIndex,int nDataIndex=3,int nParityIndex=0,int nStopIndex=0,int nHandshakingIndex=0);

public:
	CRichEditCtrl m_ctlRecieved;
	void DisplayData (LPCTSTR pszData);
	CString m_nBaudIndex;
	CString m_strSendMessage;
	afx_msg void OnBnClickedButClear();
	CString m_strCom;
	BOOL m_bAuto;
private:
	DWORD SendData(CString strDataForSend);
	char m_szSerialBuffer[401]; //Buffer for a one command from control box
	char * m_pCurrent;			 //Current pointer in buffer
	//if expecting a response
	BOOL m_bExptResp;
	BYTE m_btPacketBuffer[512];
	BYTE m_btCrcBuffer[16];


	//File related variables
	CString m_strFilename;
	CFile	m_fSourceFile;
	BOOL	m_bFileOpen;

	DWORD	m_nFileLength; //File total size in byte
	DWORD	m_nLengthAsPage;
	DWORD	m_nLengthAsSector;

	WORD	m_nPackSeq;		//packet/page sequence in total
	WORD	m_nSecSeq;		//sector sequence
	DWORD	m_nCurrentReadSize; //The size of one reading, Maximum 64K

	CString m_strPacketsSent;
	WORD	m_nCrc;


public:
	afx_msg void OnEnChangeEdtAccel();
	afx_msg void OnBnClickedButAuto();
	CString m_strCommand;
	CString m_strParameter;
	afx_msg void OnBnClickedButSendcmd();
	afx_msg void OnBnClickedButSendt();
	int SendNextPacket(void);
	afx_msg void OnBnClickedButFile();

	FILE	m_hFile;
	int m_nParam2;
	//Params
	CString m_strP1,m_strP2,m_strP3;
	DWORD m_nIp,m_nNetMask,m_nGateway;
	CString m_strMac;



private:
	// Move sector pointer, refresh data buffer from file in case
	int GoNextCheck(void);
public:
	afx_msg void OnBnClickedButHelp();
	int SendWhole(WORD nStartSector);
//	afx_msg void OnStnClickedStaticFile();
	afx_msg void OnBnClickedButSendt2();
	afx_msg void OnBnClickedButParam();
};
