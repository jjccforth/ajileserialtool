#ifdef __cplusplus
extern "C" {  // only need to export C interface if
              // used by C++ source code
#endif

void init_crc16_tab(void);
void update_crc_16(unsigned short *crc, char c);

#ifdef __cplusplus
}
#endif