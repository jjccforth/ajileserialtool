//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by SerailDialog.rc
//
#define ID_BUT_CLEAR                    3
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDR_MAINFRAME                   128
#define IDD_COM_SETTINGS                130
#define IDD_COM_EVENTS                  131
#define IDD_COM_SELECT                  132
#define IDD_SERAILDIALOG_DIALOG         142
#define IDD_PARAMETERS                  143
#define IDC_BAUDRATE                    1000
#define IDC_BUT_CONNECT                 1000
#define IDC_BAUDRATE_1200               1001
#define IDC_BUT_DISCONNECT              1001
#define IDC_BAUDRATE_2400               1002
#define IDC_BUT_SEND                    1002
#define IDC_BUT_AUTO                    1003
#define IDC_BAUDRATE_9600               1003
#define IDC_BAUDRATE_14400              1004
#define IDC_BUT_TOTAL                   1004
#define IDC_BAUDRATE_19200              1005
#define IDC_RICHEDIT_RECV               1005
#define IDC_LIST_BAUD                   1006
#define IDC_BAUDRATE_38400              1006
#define IDC_BAUDRATE_56000              1007
#define IDC_EDIT_SEND                   1007
#define IDC_BAUDRATE_57600              1008
#define IDC_CMB_COM                     1008
#define IDC_BAUDRATE_115200             1009
#define IDC_LIST1                       1009
#define IDC_CMB_CMD                     1009
#define IDC_DATA                        1010
#define IDC_EDIT_PARAMETER              1010
#define IDC_DATA_5                      1011
#define IDC_EDIT_PARAMETER2             1011
#define IDC_DATA_6                      1012
#define IDC_SENDDATA                    1012
#define IDC_DATA_7                      1013
#define IDC_EDT_STEPS                   1013
#define IDC_DATA_8                      1014
#define IDC_RELAY                       1014
#define IDC_CHECK1                      1015
#define IDC_CHECK2                      1016
#define IDC_BUT_SENDCMD                 1016
#define IDC_EDT_SPEED                   1017
#define IDC_BUT_SENDT                   1017
#define IDC_BUT_FILE                    1018
#define IDC_EDT_TOTAL                   1019
#define IDC_STATIC_FILE                 1019
#define IDC_PARITY                      1020
#define IDC_BUT_FILE2                   1020
#define IDC_BUT_SENDT2                  1020
#define IDC_PARITY_NONE                 1021
#define IDC_STATIC_PACKSENT             1021
#define IDC_PARITY_ODD                  1022
#define IDC_BUTTON1                     1022
#define IDC_BUT_HELP                    1022
#define IDC_BUT_GET                     1022
#define IDC_PARITY_EVEN                 1023
#define IDC_BUT_PARAM                   1023
#define IDC_IPADDRESS1                  1023
#define IDC_PARITY_MARK                 1024
#define IDC_EDIT1                       1024
#define IDC_PARITY_SPACE                1025
#define IDC_EDIT2                       1025
#define IDC_IPADDRESS2                  1026
#define IDC_EDIT3                       1027
#define IDC_IPADDRESS3                  1028
#define IDC_IPADDRESS4                  1029
#define IDC_STOP                        1030
#define IDC_EDIT4                       1030
#define IDC_STOP_1                      1031
#define IDC_BUT_GET2                    1031
#define IDC_BUT_SET                     1031
#define IDC_STOP_15                     1032
#define IDC_STOP_2                      1033
#define IDC_HANDSHAKING                 1040
#define IDC_HANDSHAKING_OFF             1041
#define IDC_HANDSHAKING_SOFTWARE        1042
#define IDC_HANDSHAKING_HARDWARE        1043
#define IDC_EVENTS                      1100
#define IDC_EVENT_BREAK                 1101
#define IDC_EVENT_CTS                   1102
#define IDC_EVENT_DSR                   1103
#define IDC_EVENT_ERROR                 1104
#define IDC_EVENT_RING                  1105
#define IDC_EVENT_RLSD                  1106
#define IDC_EVENT_RECV                  1107
#define IDC_EVENT_RCV_EV                1108
#define IDC_EVENT_SEND                  1109
#define IDC_EVENT_CHAR                  1110
#define IDC_EVENT_CHAR_TEXT             1111
#define IDC_EVENT_CHAR_VALUE            1112
#define IDC_COM1                        1200
#define IDC_COM2                        1201
#define IDC_COM3                        1202
#define IDC_COM4                        1203

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        144
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1031
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
